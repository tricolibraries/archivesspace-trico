 <script>
  var app = angular.module('slider', [])

  app.config(function($sceDelegateProvider) {
    $sceDelegateProvider.resourceUrlWhitelist([
					       'self',
      '/assets/slider.html'
    ]);
  });
  </script>

  <script>
  $( document ).on( "show.bs.collapse", "#bs-example-navbar-collapse-2", function() {
     $('body').addClass('menu-slider');
  });
  $( document ).on( "shown.bs.collapse", "#bs-example-navbar-collapse-2", function() {
     $('body').addClass('in');
  });
  $( document ).on( "shown.bs.collapse", "#bs-example-navbar-collapse-2", function() {
     $('#researchGuides').addClass("active");
  });
  $( document ).on( "hide.bs.collapse", "#bs-example-navbar-collapse-2", function() {
     $('body').removeClass('menu-slider');
  });
  $( document ).on( "hidden.bs.collapse", "#bs-example-navbar-collapse-2", function() {
     $('body').removeClass('in');
  });

  </script>
